package plt;

public class Translator {

	private String phrase;
	private String outputString;
	public static final String NIL ="nil";
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}
	
	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		
	if (!phrase.equals("")) {
		outputString="";
		String[] words = phrase.split(" ");
		
		for(int i=0; i<words.length; i++) {
			if(words[i].split("-").length>1) {
				String[] linewords = words[i].split("-");
				translatelinewords(linewords);
				continue;
			} 
			phrase=words[i];
		
			translateWordsStartingVowels();
			translateWordsStartingConsonants();
			if(words.length>1 && words.length!=i+1) {
					outputString = outputString + " ";
			}
		}
		return outputString;
	}	
		return NIL;	
	}
	
	
	private void translateWordsStartingVowels() {
		if(startWithVowel()) {
			if(phrase.endsWith("y")) {
				phrase= phrase + "nay";
				phrase=controlloPunteggiatura(phrase);
				outputString= outputString + phrase;
			} else if(endWithVowel()) {
				phrase= phrase + "yay";
				phrase=controlloPunteggiatura(phrase);
				outputString= outputString + phrase;
			} else if(endWithConsonant()) {
				phrase= phrase + "ay";
				phrase= controlloPunteggiatura(phrase);
				outputString= outputString + phrase;
			} 
			
		}
	}
	
	private void translateWordsStartingConsonants() {
		if(startWithMultipleConsonant()) {
			String appendstring ="";
			while(!startWithVowel()) {
				appendstring = appendstring + phrase.charAt(0);
				phrase= phrase.substring(1);
			}
		 
		phrase= phrase + appendstring;
		phrase= phrase + "ay";
		phrase= controlloPunteggiatura(phrase);
		outputString= outputString + phrase;
		
		}
	}
	
	private void translatelinewords(String[] linewords) {
		for(int j=0; j<linewords.length;j++) {
			phrase=linewords[j];
			translateWordsStartingVowels();
			translateWordsStartingConsonants();
			if(linewords.length>1 && linewords.length!=j+1) {
				 outputString = outputString + "-";
				
			}
		}
	}
	
	private String controlloPunteggiatura(String parola) {
		
		if(parola.indexOf("!")!=-1) {
			parola=parola.replace("!", "");
			parola = parola + "!";
			}
		if(parola.indexOf(",")!=-1) {
			parola=parola.replace(",", "");
			parola = parola + ",";
			}
		if(parola.indexOf(".")!=-1) {
			parola=parola.replace(".", "");
			parola = parola + ".";
			}
		if(parola.indexOf(":")!=-1) {
			parola=parola.replace(":", "");
			parola = parola + ":";
			}
		if(parola.indexOf(";")!=-1) {
			parola=parola.replace(";", "");
			parola = parola + ";";
			}
		if(parola.indexOf("?")!=-1) {
			parola=parola.replace("?", "");
			parola = parola + "?";
			}
		if(parola.indexOf("'")!=-1) {
			parola=parola.replace("'", "");
			parola = parola + "'";
			}
		if(parola.indexOf("(")!=-1) {
			parola=parola.replace("(", "");
			parola = parola + "(";
			}
		return parola;
	}
	
	private boolean startWithMultipleConsonant() {
		if(!startWithVowel() && !phrase.equals("")) {		
			return true;
		}
		return false;
	}
	private boolean startWithVowel() {
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");
	}
	
	private boolean endWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
	}
	
	private boolean endWithConsonant() {
		return !(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	
}